import { HostConfig } from './app';

export type AccountConfig = {
  email: string;
  pass: string;
  smtp: HostConfig;
  pop3: HostConfig;
  imap: HostConfig;
}

const config163 = {
  smtp: {
    host: 'smtp.163.com',
    port: 25,
    secure: false,
  },
  pop3: {
    host: 'pop.163.com',
    port: 110,
    secure: false,
  },
  imap: {
    host: 'imap.163.com',
    port: 993,
    secure: true,
  },
};
const config139 = {
  smtp: {
    host: 'smtp.139.com',
    port: 25,
    secure: false,
  },
  pop3: {
    host: 'pop.139.com',
    port: 110,
    secure: false,
  },
  imap: {
    host: 'imap.139.com',
    port: 993,
    secure: true,
  },
};
const configSina = {
  smtp: {
    host: 'smtp.sina.com',
    port: 25,
    secure: false,
  },
  pop3: {
    host: 'pop.sina.com',
    port: 110,
    secure: false,
  },
  imap: {
    host: 'imap.sina.com',
    port: 143,
    secure: false,
  },
};
const configAliyun = {
  smtp: {
    host: 'smtp.aliyun.com',
    port: 25,
    secure: false,
  },
  pop3: {
    host: 'pop3.aliyun.com',
    port: 110,
    secure: false,
  },
  imap: {
    host: 'imap.aliyun.com',
    port: 143,
    secure: false,
  },
};
const configQQ = {
  smtp: {
    host: 'smtp.qq.com',
    port: 25,
    secure: false,
  },
  pop3: {
    host: 'pop.qq.com',
    port: 110,
    secure: false,
  },
  imap: {
    host: 'imap.qq.com',
    port: 143,
    secure: false,
  },
};
const configXyz = {
  smtp: {
    host: '172.16.11.253',
    port: 25,
    secure: false,
  },
  pop3: {
    host: '172.16.11.253',
    port: 110,
    secure: false,
  },
  imap: {
    host: '172.16.11.253',
    port: 143,
    secure: false,
  },
};
const configOutlook = {
  smtp: {
    host: 'outlook.office365.com',
    port: 25,
    secure: true,
  },
  pop3: {
    host: 'outlook.office365.com',
    port: 995,
    secure: true,
  },
  imap: {
    host: 'outlook.office365.com',
    port: 993,
    secure: true,
  },
};

export const configList: AccountConfig[] = [
  {
    email: 'x1@a.xyz',
    pass: '123',
    ...configXyz
  },
];


export const storage = new LocalStorage({
  accounts: configList.map(c => c.email),
  currentAccount: configList[0]
});
